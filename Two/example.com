server { 
    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;
    server_name localhost;
    index index.php index.html index.htm;
    location /centos {
        root /var/www/centos;
    }
    location /ubuntu {
        root /var/www/ubuntu
    }
}